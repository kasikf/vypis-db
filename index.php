<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "articles";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>MyArticles</title>
</head>
<body>
    <h1>MyArticles</h1>
    <form action="" method="post">
        <div class="form-group">
            <label for="title">Vyber článek:</label>
            <select name="article-id" id="">
                <?php
                $sql = "SELECT * FROM articles";
                $result = $conn->query($sql);
                if ($result->num_rows > 0) {
                    while($row = $result->fetch_assoc()) {
                        echo "<option value=".$row["id"].">".$row["nameArticles"]."</option>";
                    }
                }
                ?>
            </select>
        </div>
        <input type="submit" value="Vyhledat">
    </form>

    <?php
        if (isset($_POST['article-id'])) {
            $article_id = $_POST['article-id'];
            $sql = "SELECT * FROM articles WHERE id = $article_id";
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) {
                    echo "<div class='article-container'>";
                    echo "<h2>$row[nameArticles]</h2>";
                    echo $row['contentArticles'];
                    echo "</div>";
                }
            }
        }
      ?>
</body>
</html>